# Work-around for the Ghostscript bugs found in August 2018 (http://openwall.com/lists/oss-security/2018/08/21/2). 
# Disables PS/EPS/PDF/PS in ImageMagick basicly so it makes it harder to be exploited by an webserver vector.
# Tested on RHEL 7 and 6

augeas { "Ghostscript-disable-PS":
  lens     => 'Xml.lns',
  incl     => '/etc/ImageMagick/policy.xml',
  context  => '/files/etc/ImageMagick/policy.xml/policymap/',
  changes  => [
                "set #text[last()+1]/ '  '",
                "set policy[last()+1]/ #empty",
                "set policy[last()]/#attribute/domain coder",
                "set policy[last()]/#attribute/rights none",
                "set policy[last()]/#attribute/pattern PS",
              ],
   onlyif  => "match policy/*/pattern[.='PS'] size == 0",
}
     
augeas { "Ghostscript-disable-EPS":
  lens     => 'Xml.lns',
  incl     => '/etc/ImageMagick/policy.xml',
  context  => '/files/etc/ImageMagick/policy.xml/policymap/',
  changes  => [
                 "set #text[last()+1]/ '  '",
                 "set policy[last()+1]/ #empty",
                 "set policy[last()]/#attribute/domain coder",
                 "set policy[last()]/#attribute/rights none",
                 "set policy[last()]/#attribute/pattern EPS",
               ],
  onlyif   => "match policy/*/pattern[.='EPS'] size == 0",
}
     
augeas { "Ghostscript-disable-PDF":
  lens     => 'Xml.lns',
  incl     => '/etc/ImageMagick/policy.xml',
  context  => '/files/etc/ImageMagick/policy.xml/policymap/',
  changes  => [
                 "set #text[last()+1]/ '  '",
                 "set policy[last()+1]/ #empty",
                 "set policy[last()]/#attribute/domain coder",
                 "set policy[last()]/#attribute/rights none",
                 "set policy[last()]/#attribute/pattern PDF",
               ],
  onlyif   => "match policy/*/pattern[.='PDF'] size == 0",
}

augeas { "Ghostscript-disable-XPS":
  lens     => 'Xml.lns',
  incl     => '/etc/ImageMagick/policy.xml',
  context  => '/files/etc/ImageMagick/policy.xml/policymap/',
  changes  => [
                 "set #text[last()+1]/ '  '",
                 "set policy[last()+1]/ #empty",
                 "set policy[last()]/#attribute/domain coder",
                 "set policy[last()]/#attribute/rights none",
                 "set policy[last()]/#attribute/pattern XPS",
               ],
   onlyif   => "match policy/*/pattern[.='XPS'] size == 0",
}